#!/bin/sh

case $1 in
	start) 
		docker build . -t pruebarunner:latest
		docker run --name pruebarunner1 pruebarunner:latest
		;;
	stop)
		ID=$(docker ps -f name=pruebarunner1 --format "{{.ID}}")
		docker stop $ID
		docker rm $ID
		;;
	*)
		echo 'start|stop'
esac
