FROM python:3-alpine
# RUN apk --no-cache add postgresql-client
ENV PYTHONUNBUFFERED 1

WORKDIR /opt/app

COPY requirements.txt /opt/app/
RUN pip install --no-cache-dir -r requirements.txt

COPY . /opt/app/

# For Django
# EXPOSE 8000
# CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]

# For some other command
# CMD ["python", "app.py"]
