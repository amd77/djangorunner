#!/bin/sh

log () {
    echo ----------------------------------------------------------------------
    echo $*
}

set -e

test -e /usr/bin/virtualenv || {
	pip install virtualenv
}
test -e .venv || {
    log creando virtualenv .venv
    virtualenv .venv --python=python3
}

log Activando virtualenv
. .venv/bin/activate

python -V

log Instalando virtualenv
pip3 install -r requirements.txt 

log Aplicando migraciones
./manage.py migrate

SUPERUSERS=$(./manage.py shell -c 'from django.contrib.auth.models import User; print(User.objects.filter(is_superuser=True).count())')
test $SUPERUSERS = 0 && {
    log Creando superusuario
    ./manage.py createsuperuser --username admin --email root@galotecnia.com --noinput
}

log ./manage.py check
./manage.py check

log ./manage.py $*
./manage.py $*
